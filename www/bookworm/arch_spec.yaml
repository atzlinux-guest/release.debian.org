# vim:set et sw=4:

amd64:
    available: yes
    portbox: barriere
    users: 23000+
    installer: d-i
    archive-coverage: 99
    archive-uptodate: 99
    upstream-support: yes
    buildds:
        - x86-conova-01
        - x86-csail-01
        - x86-grnet-01
        - x86-ubc-01
        - x86-ubc-02
    buildd-dsa: yes
    autopkgtest: yes
    concerns-rm: no
    concerns-srm:
    concerns-dsa: no
    concerns-sec: no
    candidate: yes

arm64:
    available: yes
    portbox: amdahl
    porters:
        - Vagrant Cascadian
        - Wookey
    users: "?"
    installer: d-i
    archive-coverage: 99
    archive-uptodate: 99
    upstream-support: yes
    buildds:
        - arm-arm-01
        - arm-arm-03
        - arm-arm-04
        - arm-conova-01
        - arm-conova-02
        - arm-ubc-01
        - arm-ubc-02
        - arm-ubc-03
    buildd-dsa: yes
    autopkgtest: yes
    concerns-rm: no
    concerns-srm:
    concerns-dsa: "yes: unstable and ageing hardware"
    concerns-sec: no
    candidate: "?"

armel:
    available: yes
    portbox: abel
    porters:
        - Adrian Bunk
        - Wookey
    users: "?"
    installer: d-i
    archive-coverage: 97
    archive-uptodate: 99
    upstream-support: "glibc has trouble finding qualified persons to implement security fixes,<br>
        might be special because the use of the libatomics library is mandatory"
    buildds:
        - antheil
        - arm-ubc-04
        - arm-ubc-05
        - arm-ubc-06
        - hartmann
        - hasse
        - henze
        - hoiby
    buildd-dsa: yes
    autopkgtest: yes
    concerns-rm: "some builds are hitting the address space limit"
    concerns-srm:
    concerns-dsa: "yes: unstable and ageing hardware"
    concerns-sec: no
    candidate: "?"

armhf:
    available: yes
    portbox: harris
    porters:
        - Vagrant Cascadian
        - Wookey
    users: "?"
    installer: d-i
    archive-coverage: 98
    archive-uptodate: 99
    upstream-support: "glibc has trouble finding qualified persons to implement security fixes"
    buildds:
        - arm-arm-01
        - arnold
        - antheil
        - arm-ubc-04
        - arm-ubc-05
        - arm-ubc-06
        - hartmann
        - hasse
        - henze
        - hoiby
    buildd-dsa: yes
    autopkgtest: yes
    concerns-rm: "some builds are hitting the address space limit"
    concerns-srm:
    concerns-dsa: "yes: unstable and ageing hardware"
    concerns-sec: no
    candidate: "?"

i386:
    available: yes
    portbox: barriere
    porters:
        - Adrian Bunk
    users: 92000+
    installer: d-i
    archive-coverage: 98
    archive-uptodate: 99
    upstream-support: "no: missing CPU security mitigations in Linux kernel"
    buildds:
        - x86-conova-01
        - x86-csail-01
        - x86-grnet-01
        - x86-ubc-01
        - x86-ubc-02
    buildd-dsa: yes
    autopkgtest: yes
    concerns-rm: "only one porter has volunteered,<br>
        some builds are hitting the address space limit"
    concerns-srm:
    concerns-dsa: no
    concerns-sec: no
    candidate: "?"

mips64el:
    available: yes
    portbox: eller
    porters:
        - YunQiang Su
    users: "?"
    installer: d-i
    archive-coverage: 97
    archive-uptodate: 99
    upstream-support: "no: no upstream support in GCC,<br>
        unaddressed test failures in binutils"
    buildds:
        - eberlin
        - mipsel-aql-01
        - mipsel-aql-02
        - mipsel-aql-03
        - mipsel-manda-01
        - mipsel-manda-02
        - mipsel-manda-03
        - mipsel-osuosl-01
        - mipsel-osuosl-02
        - mipsel-sil-01
    buildd-dsa: yes
    autopkgtest: no
    concerns-rm: "builders are extremely slow (also per kernel team),<br>
        only one porter has volunteered,<br>
        lack of autopkgtest infrastructure,<br>
        future availability of hardware"
    concerns-srm:
    concerns-dsa: "yes: non-server-grade hardware<br>
        time sink for DSA and hosters"
    concerns-sec: no
    candidate: "?"

mipsel:
    available: yes
    portbox: eller
    porters:
        - YunQiang Su
    users: "?"
    installer: d-i
    archive-coverage: 97
    archive-uptodate: 99
    upstream-support: "no: no upstream support in GCC,<br>
        unaddressed test failures in binutils"
    buildds:
        - eberlin
        - mipsel-aql-01
        - mipsel-aql-02
        - mipsel-aql-03
        - mipsel-manda-01
        - mipsel-manda-02
        - mipsel-manda-03
        - mipsel-osuosl-01
        - mipsel-osuosl-02
        - mipsel-sil-01
    buildd-dsa: yes
    autopkgtest: no
    concerns-rm: "builders are extremely slow (also per kernel team),<br>
        only one porter has volunteered,<br>
        some builds are hitting the address space limit,<br>
        lack of autopkgtest infrastructure,<br>
        future availability of hardware"
    concerns-srm:
    concerns-dsa: "yes: non-server-grade hardware<br>
        time sink for DSA and hosters"
    concerns-sec: no
    candidate: "?"

ppc64el:
    available: yes
    portbox: plummer
    porters:
        - Frédéric Bonnard
    users: "?"
    installer: d-i
    archive-coverage: 98
    archive-uptodate: 99
    upstream-support: yes
    buildds:
        - ppc64el-osuosl-01
        - ppc64el-unicamp-01
    buildd-dsa: yes
    autopkgtest: yes
    concerns-rm: "only one porter has volunteered"
    concerns-srm:
    concerns-dsa: "hosting is not great, effectively not redundant.<br>
        Unclear what our options are regarding purchasing hardware."
    concerns-sec: no
    candidate: "?"

s390x:
    available: yes
    portbox: zelenka
    porters:
        - Dipak Zope (!DD)
        - Rajendra Kharat (!DD)
    users: "?"
    installer: d-i
    archive-coverage: 97
    archive-uptodate: 99
    upstream-support: yes
    buildds:
        - zandonai
        - zani
    buildd-dsa: yes
    autopkgtest: yes
    concerns-rm: no
    concerns-srm:
    concerns-dsa: "rely on sponsors for hardware (mild concern)"
    concerns-sec: no
    candidate: "?"

riscv64:
    available: yes
    portbox: debian-riscv64-porterbox-01
    porters:
        - Aurelien Jarno
        - Manuel A. Fernandez Montecelo
        - Adrian Bunk
        - Adam Borowski
    users: "?"
    installer: d-i
    archive-coverage: 96
    archive-uptodate: 99
    upstream-support: yes
    buildds:
        - rv-osuosl-01
        - rv-osuosl-02
        - rv-osuosl-03
        - rv-osuosl-04
        - rv-osuosl-05
        - rv-rr44-01
    buildd-dsa: no
    autopkgtest: yes
    concerns-rm: "too late"
    concerns-srm:
    concerns-dsa: "?"
    concerns-sec: "?"
    candidate: "no"
