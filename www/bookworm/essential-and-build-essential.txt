# manually added
autoconf
automake
cargo
clang (default)
cmake
debhelper
dh-(lisp|python|sequence-*|...)
gem2deb
golang-go (default)
meson
ninja-build
python3 (default)
ruby (default)
rust-debcargo
rustc
valac

# derived from (build)essentials
bash
binutils
binutils-aarch64-linux-gnu
binutils-arm-linux-gnueabi
binutils-arm-linux-gnueabihf
binutils-common
binutils-i686-linux-gnu
binutils-mips64el-linux-gnuabi64
binutils-mipsel-linux-gnu
binutils-powerpc64le-linux-gnu
binutils-s390x-linux-gnu
binutils-x86-64-linux-gnu
bzip2
coreutils
cpp (default)
dash
debianutils
dpkg
dpkg-dev
findutils
g++ (default)
gcc (default)
gcc-*-base (default)
grep
gzip
init-system-helpers
libasan8
libatomic1
libaudit-common
libaudit1
libbinutils
libbz2-1.0
libc-bin
libc-dev-bin
libc6
libc6-dev
libcap-ng0
libcc1-0
libcom-err2
libcrypt-dev
libcrypt1
libctf-nobfd0
libctf0
libdb5.3
libdebconfclient0
libdpkg-perl
libfile-find-rule-perl
libgcc-12-dev
libgcc-s1
libgdbm-compat4
libgdbm6
libgmp10
libgomp1
libgprofng0
libgssapi-krb5-2
libhwasan0
libisl23
libitm1
libjansson4
libk5crypto3
libkeyutils1
libkrb5-3
libkrb5support0
liblsan0
liblzma5
libmpc3
libmpfr6
libnsl-dev
libnsl2
libnumber-compare-perl
libpcre2-8-0
libperl* (default)
libquadmath0
libselinux1
libsmartcols1
libssl3
libstdc++-12-dev
libstdc++6
libtext-glob-perl
libtirpc-common
libtirpc-dev
libtirpc3
libtsan2
libubsan1
libzstd1
login
make
ncurses-base
ncurses-bin
patch
perl
perl-base
perl-modules-* (default)
rpcsvc-proto
sed
sysvinit-utils
tar
usrmerge
util-linux
util-linux-extra
xz-utils
zlib1g
