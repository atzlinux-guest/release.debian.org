#!/usr/bin/env python

# Copyright (c) 2009 Anthony Towns
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

import sys, yaml

########################################################### formatting helpers

def FAIL(value): return ("red",value)
def WARN(value): return ("yellow",value)
def PASS(value): return ("lime",value)

def c_truth(value):
    if value == True:
        return PASS("yes")
    elif value == False:
        return FAIL("no")
    else:
        return WARN(value)

def c_untruth(value):
    if value == True:
        return FAIL("yes")
    elif value == False:
        return PASS("no")
    else:
        return WARN(value)

def c_host(value):
    if not value: return FAIL("none")
    return PASS('<a href="http://db.debian.org/machines.cgi?host=%s";>yes</a>'%(value))

def c_list(maybe,okay):
    def c_list_f(value):
        n=len(value)
        str='<acronym title="%s">%s</acronym>' % (", ".join(value),n)
        if n < maybe: return FAIL(str)
        if n >= okay: return PASS(str)
        return WARN(str)
    return c_list_f

def c_minmax_list(min,max):
    def c_minmax_list_f(value):
        n=len(value)
        str='<acronym title="%s">%s</acronym>' % (", ".join(value),n)
        if n < min or n > max: return FAIL(str)
        return PASS(str)
    return c_minmax_list_f

def c_num(maybe,okay):
    def c_list_f(value):
        if value < maybe: return FAIL(value)
        if value >= okay: return PASS(value)
        return WARN(value)
    return c_list_f

def c_str(value):
    if not value: return FAIL("-")
    return PASS(value)

def c_installer(value):
    if not value: return FAIL("-")
    if value == "d-i":
        return PASS(value)
    return WARN(value)

##################################################################### criteria

criteria = [
    ("available",         c_truth),
    ("portbox",           c_host),
    ("porters",           c_list(2,3)),
#    ("users",             c_num(30,50)),
    ("installer",         c_installer),
    ("archive-coverage",  c_num(90,95)),
    ("archive-uptodate",  c_num(95,97)),
    ("upstream-support",  c_truth),
    ("buildds",           c_minmax_list(2,5)),
    ("buildd-redundancy", c_truth),
    ("buildd-fast-sec",   c_truth),
    ("buildd-247",        c_truth),
    ("buildd-dsa",        c_truth),
    ("concerns-rm",       c_untruth),
    ("concerns-srm",      c_untruth),
    ("concerns-dsa",      c_untruth),
    ("concerns-sec",      c_untruth),
    ("candidate",         c_truth),
]

################################################################# table output

def dump_table(info,waivers):
    arches=info.keys()
    arches.sort()

    candidacy_at_risk = {}
    
    print "<table class='arch_qualify'>"
    print "<tr><th></th>"
    for arch in arches:
        print "<th class='arch'>%s</th>" % (arch)
        candidacy_at_risk[arch] = False
    print "</tr>"

    for c,c_fn in criteria:
        print "<tr><th class='criteria'>%s</th>" % (c)
        for arch in arches:
            v=info[arch].get(c,None)

            if c=="candidate" and candidacy_at_risk[arch]:
                if v == True: v = "?"

            if v is not None:
                col,contents = c_fn(v)
            else:
                col,contents = FAIL("-")

            w = waivers.get(arch,{}).get(c,None)
            if w:
                col="cyan"
                contents += ' <a href="%s">(w)</a>' % (w)

            if col=="red":
                candidacy_at_risk[arch]=True

            print '<td bgcolor="%s">%s</td>' % (col,contents)

        print "</tr>"

    print "</table>"

######################################################################### main

def main(argv):
    if len(argv) < 2 or argv[1] == "-h":
        print "Usage: %s <arch-spec.yaml> [<waivers-spec.yaml>]" % argv[0]
        sys.exit(1)

    info = yaml.load(open(argv[1]))
    if len(argv) >= 3:
        waivers=yaml.load(open(argv[2]))
    else:
        waivers={}

    dump_table(info,waivers)

if __name__ == "__main__":
    main(sys.argv)

