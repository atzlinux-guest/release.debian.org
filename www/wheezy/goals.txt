##
# This file lists the current release goals for Wheezy and additional candidates
# which may become a release goal in the future.
#
# Each release goal needs:
#  * One or two developers as advocate(s)
#  * Someone from the release team tracking the goal's status
#  * Short description explaining what needs to be done
#  * Short name used as a tag when user-tagging bug reports related to
#    this release goal
# The number of bugs and a way to prevent further problems of the same
# kind should also be listed.
##

# ###################
#
# NEEDS CHECKING FOR WHEEZY
#
# ###################


# RELEASE GOALS
# =============

# multiarch
  Advocate: Steve Langasek
  State: confirmed
  Wiki: http://wiki.debian.org/ReleaseGoals/MultiArch

# kFreeBSD
  # old goal was release arch; done. new goal?
  Advocate: Aurelien Jarno and Cyril Brulebois
  State: confirmed
  Wiki: http://wiki.debian.org/ReleaseGoals/kFreeBSD

# bootperformance
  Advocate: Petter Reinholdsen and Luk Claes
  State: completed
  Wiki: http://wiki.debian.org/ReleaseGoals/BootPerformance

# package quality
  Advocate: Holger Levsen and Luk Claes
  State: confirmed
  Wiki: http://wiki.debian.org/ReleaseGoals/PackagesQuality

# remove obsolete libraries
  Advocate: Barry deFreese and Luk Claes
  State: done
  Wiki: http://wiki.debian.org/ReleaseGoals/RemoveOldLibs

# full IPv6 support
  Advocate: Martin Zobel-Helas
  State: confirmed (inherited from etch)
  Wiki: http://wiki.debian.org/ReleaseGoals/FullIPv6Support
  
# full large file support (LFS)
  Advocate: ??
  State: confirmed (inherited from etch)
  Wiki: http://wiki.debian.org/ReleaseGoals/LFS

# new source package format support
  Advocate: Raphael Hertzog
  State: done
  Wiki: http://wiki.debian.org/ReleaseGoals/NewDebFormats

# .la file removal
  Advocate: Andreas Barth
  State: confirmed
  Wiki: http://wiki.debian.org/ReleaseGoals/LAFileRemoval

# CANDIDATES
# ==========

# selinux: Squeeze certifiable at EAL-4
  Advocate: Manoj Srivastava
  State: proposed
  Wiki: ?
  Request mail: http://lists.debian.org/debian-release/2009/07/msg00389.html

# DNSSec support
  Advocate: Ondrej Sury
  State: proposed
  Wiki: ?
  Request mail: http://lists.debian.org/debian-devel/2009/07/msg00950.html
