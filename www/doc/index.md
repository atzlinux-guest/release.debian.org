#! TITLE: Release Team documentation
#! SUBTITLE: quando paratus est

This is the "official" documentation for the Debian Release Team,
their processes (internal or external), etc.  It is complemented by
the [Release team page on the Debian Wiki][WIKI].

Please do not hesitate to let us know when the documentation is wrong.
This can be done by filing a bug against the `release.debian.org`
pseudo package.

The Release Team has two major but distinct work areas:

 * Preparing the next release.  This effort is lead by the Release
   Managers (RMs)
 * Maintaining the currently supported releases.  This is managed by the
   Stable Release Managers (SRMs).

[WIKI]: https://wiki.debian.org/Teams/ReleaseTeam

# Preparing the next release

During the majority of the development cycle (prior to the freeze),
the primarily work is around transitions, coordination and planning.
Once the freeze starts, the transitions stop and we start to review
changes (unblock requests).

# Maintaining the currently supported releases

Please see [Everything you ever wanted to know about Stable Release Management in Debian but were afraid to ask](stable).

