To: debian-devel-announce
Subject: Bug Squashing Party -- May 17th - 20th

Hi,

Due to numerous transitions and many new upstream versions we had a big jump in
the number of RC bugs since the Etch release.

Therefor we hereby announce the first post-Etch Bug Squashing Party (BSP) next
weekend, May 17th until May 20th. 

First a few necessary words on coordination: #debian-bugs on irc.debian.org
will be the main channel for overall coordination. Do not hesitate to ask there
before tackling any task; there will certainly be people available that can
advise against tackling problematic tasks or tasks already worked on, and give
general advice.

Unlike before the release of Etch there should be plenty of bugs for all levels
of complexity, programming language skill, now. So if you never attended a BSP
before and you want to help, _now_ would be the right time to start. 

We would like to at least solve all the remaining bugs about postrm scripts
which depend on non-essential packages [0].

When working on specific bugs, please claim them for you. This can be done by
using the "bts" command from the command line, 'bts claim 123456'. A list of
already claimed bugs can be found at [1].  Tasks that can not be expressed by a
claim but are big enough to warrant a notice for others should be noted on
http://wiki.debian.org/OngoingBSP or pages linked from there.

The RC Bug Squashing HOWTO [2] by Steve Langasek gives a good introduction and
is probably a must-read for beginners. But don't hesitate to ask questions in
#debian-bugs, we will be happy to get you to do some work :-)

Sponsoring of uploads should be no problem, the fastest way is probably to send
the complete NMU patch to the BTS, making the full source package available for
download somewhere, and asking for sponsoring on #debian-bugs.

See also the end of this mail for a list of useful links.

Procedures:
During the BSP we should use a 0-Day NMU policy again, that means uploads that
fix RC bugs that are more than a week old can be uploaded directly. This worked
reasonably well during the Etch release process so we see no reason not to use
it again. If you feel unsure about a patch and/or it the patch is rather
invasive, please consider asking on #debian-bugs for review and/or giving the
maintainer some time to react by uploading to the DELAYED queue [3].


Happy hacking and a nice weekend,

Martin Zobel-Helas and Luk Claes

[0] http://bugs.debian.org/cgi-bin/pkgreport.cgi?users=abi@debian.org;tag=postrm-depends-nonessential
[1] http://bugs.debian.org/cgi-bin/pkgreport.cgi?usertag=bugsquash@qa.debian.org
[2] http://people.debian.org/~vorlon/rc-bugsquashing.html
[3] http://people.debian.org/~djpig/delayed.html

Other useful links:
(taken from http://people.debian.org/~vorlon/rc-bugsquashing-urls.txt)

where to find available RC bugs:
  http://bts.turmzimmer.net/details.php?ignore=sid&ignnew=on&new=5

build logs for failed builds:
  http://buildd.debian.org/

Explanations for missing binaries:
  http://people.debian.org/~igloo/status.php

List of open RC bugs for a given source package:
  http://bugs.debian.org/src:<sourcepackagename>

Information on opening new bug reports:
  http://bugs.debian.org/

Debian Policy:
  http://www.debian.org/doc/debian-policy/

