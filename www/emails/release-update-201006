Hi,

As promised, another regular(ish) update on the state of the Squeeze
release and our plans for the remainder of the cycle.

First of all, some news that some of you may already have guessed.
We're delighted to be able to announce that Mehdi Dogguy has agreed to
join the Release Team.  He has recently been working on some tools to
make it easier for us to manage transitions and we believe that he will
be a great addition to the team.

We are still looking for enthusiastic DDs who would be interested in
joining the team and helping us to make Squeeze the great release we all
believe it can be.  If you're interested, please contact us at
debian-release@lists.debian.org or on #debian-release.

Current status
==============

Since the previous update, changes to allow init scripts to run in
parallel at startup, thus making the boot process faster for many
people, have been enabled in unstable[PB], we have finished the
directfb, evince, netcdf, totem, unixodbc and vtk transitions.

We have just completed the ptlib / opal and evolution / gtkhtml
transitions.  The latter means that Squeeze will release with GNOME 2.30
along with the already transitioned KDE 4.4.3.

eglibc 2.11 was recently uploaded to unstable.  After resolving a few
initial issues we were able to migrate it to testing over the weekend.

Where next?
===========

Now that eglibc 2.11 is in testing, we will shortly be moving to using
Python 2.6 as the default Python version for Squeeze. [PY26]

Other than the Python transition, there are a few other things we aim to
complete before freezing, including updates to icu, xapian and apt and a
move to Perl 5.12, assuming that the outstanding compatibility issues are
resolved or lowered in number.[PERL]

There are also a few smaller updates which we hope to able to schedule
alongside the larger transitions.

Transition freeze
=================

As announced in the previous release update, we are now instituting a
freeze on new transitions in order to make it easier to finish
transitions without them becoming blocked by other changes in unstable.

It may be possible to include smaller updates if time allows and they do
not interfere with ongoing transitions.  If your upload would require
changes to or rebuilds of other packages, or cause other packages to be
unable to transition to testing, please consider whether doing so would
help improve the quality of Squeeze.  If you believe it would, please
contact us to discuss the issue before uploading. 

RC bugs
=======

The number of Release Critical bugs in Squeeze is continuing to move in
the right direction[RC] but there are still a larger number of RC bugs
than we can freeze with.

Please feel free to pick an RC bug or three and help resolve it, whether
that be by providing a patch, uploading an NMU or demonstrating that the
bug is not RC after all.

Scheduling
==========

Some of the transitions that have been discussed in this release update
were not planned when we published our last freeze estimate.  While
considerably improving the user experience by providing newer upstream
releases and a faster boot process, they imply that we will need some
more time to prepare Squeeze. 

We will declare Squeeze frozen once the switch to Python 2.6 as the default
version and the other transitions mentioned above are completed, which we
anticipate will be during late August.  If we all work together then we may
able to finish some transitions more quickly than our current estimates.

Thanks for your attention and your assistance in making Squeeze a great
release!

NNNN
for the Release Team

[PB] http://lists.debian.org/debian-devel-announce/2010/05/msg00009.html
[PERL]
http://bugs.debian.org/cgi-bin/pkgreport.cgi?users=debian-perl@lists.debian.org;tag=perl-5.12-transition
[PY26] 
http://bugs.debian.org/cgi-bin/pkgreport.cgi?users=debian-python@lists.debian.org;tag=python2.6
[RC] http://bugs.debian.org/release-critical/
