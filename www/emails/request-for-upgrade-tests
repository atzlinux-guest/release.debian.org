To: debian-devel-announce@lists.debian.org, debian-user@lists.debian.org
Mail-Followup-To: debian-testing@lists.debian.org
Subject: Call for upgrade testing

Hi folks,

Well, with security support in place for sarge we hoped to be sending
this message out a week or so ago, but it took longer than expected to
sort out one particularly hairy bug that was known to affect upgrades.
Now that it's fixed in testing and we can accept upgrade reports without
trying to filter out the noise from that bug, the release team is happy
to issue this official call for upgrade testers for sarge.

As in releases past, we strongly recommend that you read the release
notes before upgrading, and in particular Chapter 4, "Upgrades from
previous releases", since some aspects of the recommended upgrade path
have changed.  The preliminary release notes for sarge can be found at
<http://www.debian.org/releases/sarge/releasenotes>.


Andreas Barth has prepared an upgrade report template to help when
reporting problems with your upgrades.  If you do run into problems when
testing the upgrade path from woody to sarge, please download the
template at <http://release.debian.org/upgrade-report.html>, fill it
out, and email it to submit@bugs.debian.org.  We also welcome reports of
successful upgrades, short notes to let us know things are working for
you, or tasteful fruit baskets.


We mentioned in the freeze announcement[1] that we needed volunteers to
help with processing upgrade reports -- taking them apart, identifying
the bugs that appear, and assigning them to the packages responsible so
that they can get fixed for sarge.  Our call for volunteers got us a
total of, uh... one person offering to help, so we could probably use
more. :)  If you are an experienced user who is good at figuring out who
to blame when things break, and you have some time you'd be willing to
spend helping make sarge the best Debian release ever, please contact
debian-release@lists.debian.org.  We'll be happy to put you to work.

Cheers,
-- 
NN
Debian Release Team

[1] http://lists.debian.org/debian-devel-announce/2005/05/msg00001.html
