DRAFT DRAFT DRAFT

To: d-d-a
Subject: Bits from the Stable Release Team

Hi,

As you're probably aware, we recently released the latest point release 
of the stable distribtion, lenny, as 5.0.4.  We thought this would be a 
good opportunity to reiterate and clarify a few aspects of point releases and 
stable updates.

Updating your package in stable
===============================

If you believe that a bug should be fixed in (old)stable, please prepare 
a diff against the version of the affected package currently in stable / 
proposed-updates and send it to debian-release@lists.debian.org together 
with an explanation of the bug.  The diff should only include changes
related to fixing the bug.

As a general rule of thumb, a patch is likely to be acceptable for a 
stable update if it:

* Fixes a bug of severity Important or higher, filed in the Debian BTS
* Has been thoroughly tested in unstable

If you're unsure whether a stable update would be appropriate, please 
feel free to send a mail anyway.

For security-related updates, the Security Team should be the first 
point of contact.  If they decide that a security issue does not warrant 
the issuing of a DSA then it will often be possible to fix the issue via 
a stable update.

proposed-updates
================

When a package destined for "stable" is uploaded, dak automatically 
moves it in to the "p-u-new" queue.  The Release Team regularly review 
the content of the queue and accept or reject packages from it.

Once a package is accepted, it will move in to the "proposed-updates" 
distribution and be submitted to the buildd network for auto-building.  
At this point, the package is publicly available from Debian mirrors by 
adding a line such as

deb http://ftp.debian.org/debian lenny-proposed-updates main contrib non-free

to sources.list.

Testing of packages in proposed-updates is appreciated, as it increases 
the chances that any regressions or new bugs introduced by the update 
will be found and corrected before the actual point release.

oldstable
=========

All of the above also applies to oldstable, replacing "stable" and 
"lenny" with "oldstable" and "etch".

Security support for etch is in the process of ending; the ability to 
update etch through proposed-updates will also end soon.  There will be a
final point release incorporating all available updates from
security.debian.org and oldstable-proposed-updates, before etch is moved to
archive.debian.org.

Although a date has not yet been set for this point release, it is 
likely to take place in either late March or early April.

Cheers

NN

For the release team
