Subject: release update: d-i schedule, release notes, deep freeze

Hi,

You might not remember who the release team is, but we still want to
inform you about the final leg of the etch release cycle.  Our original
schedule did not work out due to problems with the kernel and the
slower than expected reduction of release critical bugs.

kernel status
=============

Thanks to the dedication of the kernel team, these kernel problems have
been resolved and we now have a releasable 2.6.18 Linux kernel in etch.
Work continues on fixing remaining usability issues in the kernel, but
the kernel is no longer a blocker for the etch release.

debian-installer release
========================

As a result, the d-i team has been able to plan ahead and do a schedule
for the release of debian-installer rc2[1], which we expect will be used
as the installer for etch.  debian-installer is currently planned to
release around the 20th of March.

timeline
========

After a few months of delay, then, this gives us enough information to
regroup and offer a new projected release timeline.  The good news is
that we have not been sitting idle for the past months; many more RC
bugs have been fixed... and found... and fixed since the last release
update, and there have been good upgrade and install reports, which
allows for some compression of the remaining tasks.

N-20 = 13 Mar 2007:

	40 RC bugs.  Freeze continues; changes to etch for
	non-release-critical issues are no longer accepted, to further
	limit the risk of incidental regressions and to better focus on
	the remaining release blockers.

N-14 = 19 Mar 2007:

	20 RC bugs.  D-I RC2 releases.  Users are encouraged to use the
	debian-installer for new installs and to begin upgrading systems
	from sarge to the security-supported etch, and report any
	problems found.

N-7  = 26 Mar 2007:

	~0 RC bugs.  Final adjustments to packages in response to
	install and upgrade feedback.  Finalization of the release
	notes.

N    =  2 Apr 2007:
	0 RC bugs.  Barring any problems that would cause us to need to
	re-roll the installer <knock on wood>, we should be ready to
	release.

release notes
=============

Now is the time for package maintainers to ask for inclusion of
information in the release notes about important differences between the
sarge and etch versions of their packages.  Since the set of non-RC bugs
that we will ship with should also now be fixed, this is also the time
to document any of those that you feel are important to mention.

Please also try to report all problems you notice when migrating sarge
to etch boxes to the release-notes pseudo-package.

shooting rc bugs in a barrel
============================

For the past few months, the release team has been fairly lenient about
letting RC bugs go unfixed in testing because we knew there was no
hurry.  Well, now we're hurrying again.  If you have a package in
testing with RC bugs, please fix it or ask for help fixing it.  At this
point, packages with RC bugs open in testing for more than 10 days
(all, uh, 10 of them?) are subject to removal by the release team, so
please take care of your packages to ensure their inclusion in etch.

At the same time, non-maintainers should consider paying attention to
any packages they use that have RC bugs[2].  The 0-day NMU policy for
RC bugs continues, now with an even further reduced wait time for new
bugs of 3 days.  This doesn't mean you should rush to beat the
maintainer to his own bugs.  If you have the maintainer's permission you
can always upload even sooner, but even though we want to see every
package bug-free and included in the release, it's still important that,
in our enthusiasm, we don't become careless with packages that someone
else will have to clean up later.

Of course, directly prior to release, members of the release team might
approve on a case-by-case-basis even shorter waiting times - just for the
simple reason that we need fix the issues prior to release. (We hope
that is not necessary, but remembering the Sarge release, we even
found a problem while spinning the CDs.)


Cheers,
-- 
Luk
Debian Release Team

[1] http://lists.debian.org/debian-boot/2007/03/msg00646.html
[2] http://bts.turmzimmer.net/details.php?bydist=etch&ignbritney=on&pseudopackages=on&ignmerged=on
