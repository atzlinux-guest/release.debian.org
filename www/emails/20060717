To: debian-devel-announce@lists.debian.org
Subject: release update: Etch 4.0, Blockers and Goals, Arch status, kernel 2.4, etc
From: Marc Brockschmidt <he@debian.org>
Organization: The Debian Project
Date: Mo Jul 17 11:15:28 UTC 2006

Hi,

Since our last release update, there were a few changes we want to inform
you about - actually quite many and we should have written earlier, but
writing an update also takes time.

Please note that as of now, RC bugs and problematic transitions are our
main concern.  There has been progress, but we still need to lower the
number of release critical bugs further.  There will be a couple of Bug
Squashing Parties soon, so please consider to join one or more of them. [1]

We want to ask you to not do disturbing updates of your packages in
unstable without contacting the release team before.  If you need a staging
area or simply want to use Debian infrastructure to show newer packages,
you can always upload to experimental, which is nowadays mostly autobuilt. [2]

Etch will carry 4.0 as version number.


Release Goals
=============

There has been some misunderstanding what Release Goals are.  While
(unresolved) Release Blockers block our release (that is why they are
called Blockers), Release Goals don't.  So, Release Blockers are more
important than the timeline, while Release Goals are not.  And of course,
any bug required to be fixed for a Release Blocker automatically has (at
least) serious severity, whereas for a Release Goal, the severity is (at
least) important.  However, there is one common thing, the NMU policy.
Packages in which such a bug exists can be NMUed with a 0-days upload after
the bug is reported for at least one week (please see below for details how
to do NMUs).  (Of course, we need support from all of you to make sure the
release blockers are resolved in time, so that we can release in December.)

By giving some targets the status of "Release Goal", we give them an
official status that allows the people who drive them to go on quite
fast, without putting real danger to the release schedule.

There was a new request for another approved release goal, that is NFS
v4 support.  We approved that goal.  On the other hand, with switching
to gcc 4.1 as default, this item has disappeared from the release
goal list (as "FTBFS with gcc-4.1" is now a serious bug anyways with
the normal release policy).

We have these committed release blockers (but most of them are done, please
see the section about them below):
 - gcc 3.3 -> 4.* toolchain transition
 - xfree86 -> xorg transition
 - amd64 as an official arch
 - sorting out docs-in-main vs. the DFSG
 - secure apt

And these release goals currently:
- LSB 3.1 compatibility
- SELinux support
- pervasive ipv6 support
- pervasive LFS (large files) support
- new python framework
- NFS v4 support



Python
======

The new python framework has been uploaded to unstable and has propagated
to testing.  Currently, python maintainers and helpful developers are
updating all packages for the new python policy. If you maintain a package
and haven't updated it yet, do it now [3] or ask for help (but of course make
sure you don't disturb an ongoing transition, i.e. be careful if you have
different versions of your package in testing and unstable).


Kernel
======

There has been some discussions with the kernel and d-i team about the
kernel version(s) for etch.  Though there is no final agreement, current
tendencies are to ship etch with 2.6.17 or newer.
The kernel will be frozen on July 30th or shortly after, but there will be
(at least) one chance to put ABI-changing kernel changes or even a newer
kernel into etch in mid of October, shortly before building the final
installer for etch.  Currently, due to the massive number of local root
exploits there is some delay relative to the original time line, but it
looks like this is not going to delay the final release.  We are currently
sorting that out with the kernel- and installer-team.

Kernel version 2.2 will not be supported any more for any architecture.
Kernel version 2.4 is deprecated and won't be shipped as part of etch,
but user space applications will have to deal somehow with 2.4 kernels
(because of upgrade path, local installations, ...).


Architectures
=============

We reviewed the architecture status once again in middle of June.

All release architectures that were listed as target for etch in our
last releases are still good to go.  However, there will be more
reviews in the next months.

There has been progress with s390, we now have enough porters for this
architecture.  Therefore, s390 is again in the release set.  For sparc,
there used to be kernel failures on the buildds.  Now an appropriate
patch exists, but we have yet to figure out how it is or will be
included into Debian.  Though that is not final, we are confident enough
to also set back sparc to an release architecture (especially as the
bugs don't negatively affect any other architecture - the worst that can
happen is that we don't have a proper kernel on sparc and need to review
this decision again).

However, even for the "good enough"-architectures, there are sometimes bad
issues around.  Our testing migration scripts normally only ignore
breakages on certain architectures, which are as of now sparc and m68k.
However, in the case of getting a problematic transition through, we
started to ignore smaller breakages also on other architectures if there
was no easy way to avoid it, except on i386, powerpc and amd64.  While it
is the release team's task to make sure testing is releasable at all, we
need the porters to fix architecture specific issues.  The current status
looks good on most architectures, but on arm, hppa, s390 and sparc, there
is some porter work necessary.  Statistics of what is uninstallable are
available on http://release.debian.org/broken/

The most problematic architecture is definitely m68k; more about that in the
next release update.


X.org transition
================

Done. Yay.


Non-essential toolchain
=======================

There was the question what "non-essential toolchain" is. Basically,
that includes all packages which have influence on how other packages
build, and that are not already part of the essential toolchain. Some
packages that qualify for this are: bison, cdbs, python2.4 (and related
packages), debhelper, gcj.


Timeline
========

Reviewing our old schedule:

         Thu 15 Jun 06: (a month ago)
         
    last chance to switch to gcc 4.1, python 2.4
    [ switch happened/happening, only some issues left ]
    review architectures one more time
    [ Done, added s390, sparc to release architectures again ]
    last chance to add new architectures
    [ Nothing ]

    RC bug count less than 300
    [ around ~275 for two weeks now. Actually ~390 RC bugs in testing (due
      to fixes waiting for the transition) ]

Now to our next steps (and with more BSPs to come):

N-117  = Mon 30 Jul 06:

    freeze essential toolchain, kernels
    [ kernel freeze is probably a bit delayed ]

    RC bug count less than 200

    
N-110  = Mon  7 Aug 06:

    freeze base, non-essential toolchain (including e.g. cdbs)
    review architectures one more time (only remove broken archs)

    RC bug count less than 180


    Fri 11 - Sun 13 Aug 06:

    real-life BSP in G�tersloh, Germany, and online BSP world-wide
    
    
N-105  = Mon 14 Aug 06:

    d-i RC [directly after base freeze]

    RC bug count less than 170

    
    Fri 8 - Sun 10 Sep 06:

    real-life BSP in Wien (Vienna), Austria, and online BSP world-wide
    
    
    Fri 6 - Sun 8 Oct 06:

    real-life BSP in Espace Autog�r� des Tanneries, Dijon, France,
    and online BSP world-wide
    
    
N-45   = Wed 18 Oct 06:

    general freeze [about 2 months after base freeze, d-i RC]
    review architectures last time (only remove broken archs)
    final d-i build; chance to change the kernel version

    RC bug count less than 80

    
N      = Mon  4 Dec 06:
    release [1.5 months for the general freeze]

    no RC bugs left!



RC bug count
============

Many uncoordinated uploads are still being made to unstable which break
other packages and delay testing propagation.  We see more and more
maintainers to switch gears, and focus on fixing existing breakages.
But we need help from you all, the release is a common act.  So, please
stop making disruptive uploads, and work on getting things smoother now.

The RC bug tracker shows as of today about 390/275 release-critical bugs -
that is way too much. So, we ask you all to work on reducing the bug
number again.  At this point, we want to remember you that we have a
permanent BSP: You can upload 0-days NMUs for RC-bugs open for more than
one week.  However, you are still required to notify the maintainer via BTS
before uploading.  And of course, you need to take care of anything you
broke by your NMU.  Please upload security bug fixes with urgency high,
and other RC bug fixes with urgency medium - as of writing this, we have
almost 120 bug fixes waiting for testing propagation, which is a bit
too much.

Please see the Developers Reference for more details about NMUing [4].


Release blockers
================

Still open from our release blockers list:

 - amd64 as an official arch (and the mirror split as a pre-condition
   for that)
Almost done - 13 uninstallable packages on amd64 are waiting for the new
neon version, which requires to resolve perl's FTBFS, and resolving RC-bugs
on ntp, etc. Nothing critical any more.

 - sorting out docs-in-main vs. the DFSG
Has happened partially - the GR changed the GFDL-situation a bit. Some more
work here would be welcome.

 - sorting out non-free firmware
There is at least one issue, qlogic FC host firmwares, which are needed
to initialise the devices. One cannot access the attached storage without it.
Unfortunately, there we still need some more development time. Though this
doesn't sound like a large issue, it has the potential to delay release
(especially as the installer needs access to it, and due to a bug in
dak process_new we can have udebs only in main, but not in contrib nor
non-free.

 - secure apt
secure apt is now part of testing.  However, we need to do something for key
management etc - so some small issues need to be resolved.


So much for now.  Thanks for your support.


Cheers,
-- 
Marc Brockschmidt
Debian Release Team

[1] BSPs around the world: http://wiki.debian.org/BSPMarathon

[2] http://lists.debian.org/debian-devel-announce/2006/04/msg00007.html

[3] Informations about the new python policy:
    http://wiki.debian.org/DebianPython/NewPolicy

[4] http://www.debian.org/doc/developers-reference/ch-pkgs.en.html#s-nmu
