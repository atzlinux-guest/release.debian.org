From: Colin Watson <cjwatson@debian.org>
To: debian-devel-announce@lists.debian.org
Subject: Release update: base and standard frozen

"You're only supposed to blow the bloody doors off!"

As of last night, thanks to Daniel Silverstone, no more base (as
installed by debootstrap) or standard (by priority) packages will be
accepted into testing from unstable.

Fixes for RC and important bugs, as well as updated package
translations, are still allowed through uploads to
testing-proposed-updates. These uploads will be manually reviewed and
approved by the release team; we would appreciate, even more than usual,
that diffs be kept to an absolute minimum so that our job is easier.
Please review your changes with 'debdiff' or similar before uploading.
If in doubt, get in touch with debian-release@lists.debian.org first.

Uploads of gcc-3.3 and gcc-3.4 are currently waiting in
testing-proposed-updates/incoming for builds to complete. These will
allow us to make packages built against unstable that depend on libgcc1
valid candidates for testing.

As for the other major library transition that's happening, namely
libtiff4, it seems to be well in hand thanks to some sterling efforts.
The last few updates to unstable should happen over the next couple of
days.

Now, we have slipped a little bit, partly because of toolchain problems
and partly because debian-installer needed a few extra days anyway, so
here's an updated timeline:

  7 August 2004
  215 RC bugs
  Hard freeze of base+standard

Here we are. We need to be fixing RC bugs as quickly as possible from
now on, and RC bugs should not be staying open for longer than a week.
If you're having trouble, contact debian-release or ask for help in
#debian-bugs on irc.freenode.net.

If any of the buildds are still having problems with
testing-proposed-updates or testing-security uploads, we're going to
need those to be fixed soon.

Base and standard libraries in unstable may not make changes that
require increased shared library dependencies. This is in order that
lower-priority packages built against them can still propagate to
testing.

  7 August 2004
  215 RC bugs
  Debian-installer RC1 released

Also today, businesscard and netinst CD images of d-i RC1 have been
built, and full CD sets are building. These should be available by
dinstall time on 7 August. We now need developers and interested users
to begin testing these images for release-worthiness, and reporting both
successes and failures as bugs against the installation-reports package
in the usual way. The release schedule allows time for another d-i
release in late August to fix any release-critical problems found.

The full CD sets (as opposed to businesscard and netinst images) may
still need a lot of work.

Based on feedback from d-i testing, the installation manual is refined
and prepared for release.

  12 August 2004
  ~180 RC bugs
  testing-proposed-updates, testing-security working for all
  architectures
  Official security support for sarge begins

Assuming the toolchain is in order, we can hope to have autobuilders for
testing-proposed-updates and testing-security in working order for all
architectures by this point. The testing-proposed-updates queue will
already be in use for uploads of RC bugfixes, but up to now
testing-security is not in use. Now that it's ready, the security team
can begin providing security support for sarge. The sooner this can
actually happen, the better.

The biggest chunk of the security team's load is going to be in
searching for regressions from woody, which is something that can easily
be distributed over developers and users. Volunteers to help with this
would be greatly appreciated.

With security support in place, adventurous users can begin testing the
upgrade path from woody to sarge. Their feedback will be used for
further bugfixing of packages, and to start preparing release notes.
Please report any problems as bugs against the upgrade-reports package.

  17 August 2004
  ~165 RC bugs
  Last call for low-urgency uploads

  28 August 2004
  100 RC bugs
  d-i RC2 if needed
  Freeze time!

  16 September 2004
  0 RC bugs
  Evaluate status and pick date for release

  19 September 2004
  Release target

You know the rest. Over the next two or three weeks, we need to
concentrate hard on stabilizing the system and fixing the 200-odd
release-critical bugs that remain, either by making minimal changes to
packages or by removing them.

Cheers,

-- 
Colin Watson                                       [cjwatson@debian.org]
Debian Release Team
