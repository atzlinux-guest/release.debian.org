Subject: bits from the release team

Hi,

we are now basically 7 months away from the release of Etch.  This means,
we must now start to send out monthly release updates again, and we must
definitely work more on managing the release in time.

For this reason, we finally managed to contact our potential new Release
Assistants.  We gave them tasks, and see who survives (or as one put it
"this is Tasks and Skills for release assistants" :).  You will soon notice
that there are now more people to take care of the release process.

Which architectures will be released with Etch has not yet been 
finalized, but of course
as we are getting nearer to release, changes are less likely.  There is
one change to last status: Arm now qualifies as a release architecture again.
Congratulations to the arm porter team for that.  We will re-evaluate
the architectures twice again before release of Etch, this is about middle of
June and about end of July when we start to freeze.  We reserve the right to
remove an architecture even later in the release process if it becomes too
bad after that. Another news is that amd64 is now in unstable and is
currently being added to testing.


Now, let's look forward what we expect to see in 2006: We expect to release
Etch as planned in beginning of December 2006.[1]


Our time line still is:

N-117  = Mon 30 Jul 06: freeze essential toolchain, kernels
N-110  = Mon  7 Aug 06: freeze base, non-essential toolchain (including
                        e.g. cdbs)
N-105  = Mon 14 Aug 06: d-i RC [directly after base freeze]
N-45   = Wed 18 Oct 06: general freeze [about 2 months after base
                        freeze, d-i RC]
N      = Mon  4 Dec 06: release [1.5 months for the general freeze]


It is still possible for us to reach this goal, but we need to switch gears
in order to make it happen.  Many uncoordinated uploads are still being
made to unstable which break other packages, and the RC bug count is going
up instead of down; it's time for us all to focus on making sure our
uploads help the RC bug count go in the other direction. So, please stop
making disruptive uploads, and work on getting things smoother now.

The RC bug tracker shows as of today more than 400 release critical
bugs - that is way too much. So, we ask you all to work on reducing the bug
number again.  At this point, we want to remember you that we have an
permanent BSP: You can upload 0-days NMUs for RC-bugs open for more than
one week.  However, you are still required to notify the maintainer via BTS
before uploading.  And of course, you need to take care of anything you
broke by your NMU.

Please see the Developers Reference for more details [2].


Looking at our release targets published back in October[1], we had:
 - gcc 3.3 -> 4.0 toolchain transition
 - xfree86 -> xorg transition
these two are done
 - amd64 as an official arch (and the mirror split as a pre-condition
   for that)
almost done.
 - sorting out docs-in-main vs. the DFSG
has happened partially - the GR changed the GFDL-sitution a bit. Some more
work here would be welcome.
 - sorting out non-free firmware
this is the next large blocker that needs to be addressed.
 - secure apt
secure apt is now part of testing.  However, we need to do something for key
management etc - so some small issues need to be resolved.



Also, we need to decide soon which kernel version to take for Etch.  2.6.16
has been announced as being long-term maintained, which sounds like a
candidate, but this discussion has not yet really started inside Debian (but
needs to be finished well before we freeze the kernel itself, and also
before the release of 2.6.17 in case we should want to stick with 2.6.16).



So much for now.  Thanks for your support.


Cheers,
-- 
Andi
Debian Release Team

[1] http://lists.debian.org/debian-devel-announce/2005/10/msg00004.html
[2] http://www.debian.org/doc/developers-reference/ch-pkgs.en.html#s-nmu
