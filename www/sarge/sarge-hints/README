Files in this directory are used to hint britney.

The format is:

	# comment
	hint <pkg1>/<ver1> <pkg2>/<ver2> ...
	easy <pkg1>/<ver1> <pkg2>/<ver2> ...
	force-hint <pkg1>/<ver1> <pkg2>/<ver2> ...
	remove <pkg1>/<ver1> ...
	force <pkg1>/<ver1> ...
	block <pkg1> <pkg2> <pkg3> ...
	block-all source
	approve <pkg1>/<ver1> <pkg2>/<ver2> ...
	unblock <pkg1>/<ver1> <pkg2>/<ver2> ...
	urgent <pkg1>/<ver1> <pkg2>/<ver2> ...
	finished

If any package isn't available for updating into testing at the listed
version, the line will be ignored. If the package is already up to date in
testing, it'll likewise be ignored.

"hint" lines will try updating the listed packages all at once, and then
any remaining ones one at a time, and will be committed if the end result
is at least as installable as initially.

"easy" lines update the listed lines all at once, and commit the change if
that's no worse than initially.

"force-hint" lines will update the listed packages all at once, and
ensure they are committed no matter how many dependencies are broken;
other packages that don't cause any additional break will also be updated.

"remove" lines attempt to remove the specified version of the package(s)
if it's in testing, and prevent that version from being moved from
unstable into testing. `remove foo/1.0-1 bar/1.1-2' and `remove
foo/1.0-1\nremove bar/1.1-2' have the same effect.

"block" prevents any version of a package from being upgraded

"block-all source" prevents any sourceful update

"unblock" undoes a "block" for a particular version of the package

"approve" allows an upload to t-p-u to be considered, and unblocks it if
necessary

"force" will ensure that a particular version of a package will be
considered immediately no matter what problems it may have. Dependencies
will still be checked though.

"urgent" indicates that a particular version of a package should be treated
as high urgency

"finished" tells britney to ignore everything in the file from that point on.

